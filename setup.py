#
# SL1 web configuration setup script
# Copyright 2019, Prusa Research s.r.o.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Prusa Research s.r.o.
# Partyzánská 188/7a,
# 17000 Praha 7,
# Czech Republic
#
# info@prusa3d.com
#

from setuptools import setup, find_packages

setup(
    name="wifi-config",
    version="0.5",
    packages=find_packages(),
    scripts=['wificonfig/legacy_wifi_importer.py'],
    data_files=[
        ('/usr/lib/systemd/system', ['systemd/legacy-wifi-importer.service']),
    ], install_requires=['pydbus']
)
