#!/bin/sh

# Resolve target
if [ "$#" -ne 1 ]; then
    echo "Please provide target ip as the only argument"
    exit -1
fi
target=${1}
echo "Target is ${target}"

# Print commands being executed
set -o xtrace

# Create temp root
tmp=$(mktemp --directory --tmpdir=/tmp/ wificonfig.XXXX)
echo "Local temp is ${tmp}"

echo "Running setup"
python3 setup.py sdist --dist-dir=${tmp}

# Create remote temp
target_tmp=$(ssh root@${target} "mktemp --directory --tmpdir=/tmp/ webconfig.XXXX")
echo "Remote temp is ${target_tmp}"

echo "Install on target...start"
scp -r ${tmp}/* root@${target}:${target_tmp}
ssh root@${target} "\
set -o xtrace; \
cd ${target_tmp}; \
tar xvf wifi-config*.tar.gz; \
rm wifi-config*.tar.gz; \
cd wifi-config-*; \
pip3 install . ; \
systemctl daemon-reload; \
systemctl restart legacy-wifi-importer.service
"
echo "Install on target...done"

echo "Removing remote temp"
ssh root@${target} "rm -rf ${target_tmp}"

echo "Removing local temp"
rm -rf ${tmp}
