#!/usr/bin/env python

#
# SL1 wifi configuration web server
# Copyright 2019, Prusa Research s.r.o.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Prusa Research s.r.o.
# Partyzánská 188/7a,
# 17000 Praha 7,
# Czech Republic
#
# info@prusa3d.com

"""
Shared code for legacy wifi importer and (now removed) wifi configurator
"""

import uuid
from typing import Dict

from pydbus import SystemBus, Variant

NETWORK_MANAGER = "org.freedesktop.NetworkManager"


def add_connection(connection: Dict) -> None:
    """
    Add NetworkManager connection given by dictionary

    :param connection: Connection dictionary
    :return: None
    """
    settings = SystemBus().get(NETWORK_MANAGER, "Settings")
    settings.AddConnection(connection)


def create_client_connection(ssid: str, psk: str) -> None:
    """
    Create client connection

    :param ssid: Network SSID
    :param psk: Network key
    :return: None
    """
    connection = {
        'connection': {
            'type': Variant("s", "802-11-wireless"),
            'uuid': Variant("s", str(uuid.uuid4())),
            'id': Variant("s", ssid),
            'autoconnect-retries': Variant("i", 0),
            'auth-retries': Variant("i", 0)
        },
        '802-11-wireless': {
            'ssid': Variant("ay", ssid.encode('utf-8')),
            'mode': Variant("s", 'infrastructure')
        },
        'ipv4': {
            'method': Variant("s", 'auto')
        },
        'ipv6': {
            'method': Variant("s", 'auto')
        }
    }

    if psk and psk != "":
        connection['802-11-wireless-security'] = {
            'key-mgmt': Variant("s", 'wpa-psk'),
            'auth-alg': Variant("s", 'open'),
            'psk': Variant("s", psk)
        }

    add_connection(connection)


def create_hotspot_connection(ssid, psk, auto_connect: bool = False, connection_uuid: str = str(uuid.uuid4())) -> None:
    """
    Create hotspot connection

    :param ssid: Hotspot SSID
    :param psk: Hotspot key
    :param auto_connect: Tru if atoconnect is desired, False otherwise
    :param connection_uuid: Connection uuid, defaults to new uuid
    :return: None
    """
    connection = {
        'connection': {
            'type': Variant("s", "802-11-wireless"),
            'uuid': Variant("s", connection_uuid),
            'id': Variant("s", 'hotspot'),
            'autoconnect': Variant("b", auto_connect)
        },
        '802-11-wireless': {
            'ssid': Variant("ay", ssid.encode('utf-8')),
            'mode': Variant("s", 'ap'),
            'band': Variant("s", "bg")
        },
        '802-11-wireless-security': {
            'key-mgmt': Variant("s", 'wpa-psk'),
            'psk': Variant("s", psk),
            'proto': Variant("as", ['rsn'])
        },
        'ipv4': {
            'method': Variant("s", 'shared')
        },
        'ipv6': {
            'method': Variant("s", 'shared')
        }
    }

    add_connection(connection)
