#!/usr/bin/env python

#
# SL1 wifi configuration web server
# Copyright 2019, Prusa Research s.r.o.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Prusa Research s.r.o.
# Partyzánská 188/7a,
# 17000 Praha 7,
# Czech Republic
#
# info@prusa3d.com

"""
Legacy wifi importer service

This is used to import old direct hostapd/wpa_supplicant based wifi configuration into NetworkManager
"""

import json
from pathlib import Path
from typing import Dict
from wificonfig.shared import create_client_connection, create_hotspot_connection

LEGACY_AP_CONF_PATH = Path("/etc/hostapd.secrets.json")
LEGACY_CLIENT_CONF_PATH = Path("/etc/wpa_supplicant/wpa_supplicant-wlan0.secrets.json")


def read_legacy_data(path: Path) -> Dict[str, str]:
    """
    Import legacy configuration data

    :param path: Path to import
    :return: Configuration dictionary
    """
    config = {}
    try:
        with path.open("r") as file:
            config = json.load(file)
    except FileNotFoundError:
        print(f"No legacy wifi configuration present in {path}")
    except json.JSONDecodeError:
        print(f"Failed to decode legacy wifi configuration in {path}")
    return config


def import_configs() -> None:
    """
    Import both client and hotspot configuration

    :return: None
    """
    ap_config = read_legacy_data(LEGACY_AP_CONF_PATH)
    if "ssid" in ap_config and "psk" in ap_config:
        create_hotspot_connection(ap_config["ssid"], ap_config["psk"])
        print("Imported legacy hotspot connection settings")
        LEGACY_AP_CONF_PATH.unlink()

    client_config = read_legacy_data(LEGACY_CLIENT_CONF_PATH)
    if "ssid" in client_config and "psk" in client_config:
        create_client_connection(client_config["ssid"], client_config["psk"])
        print("Imported legacy client connection settings")
        LEGACY_CLIENT_CONF_PATH.unlink()


if __name__ == '__main__':
    import_configs()
